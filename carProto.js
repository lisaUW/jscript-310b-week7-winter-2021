/**
 * Car class
 * @constructor
 * @param {String} model
 */

let Car = class {
  constructor(model) {
    this.speed = 0;
    this.model = model;
  }

  accelerate() {
    this.speed++; 
  }

  brake(){
    this.speed--;
}

  toString(){
    return this.model
  }
}

// 2. Create an instance, accelerate twice, brake once, and console log the instance.toString()

const car1 = new Car();

car1.accelerate();
car1.accelerate();
car1.brake();
console.log(car1.toString());


let ElectricCar = class Car {
  constructor( ) {
    this.motor = "electric";
  }
  accelerate(){
    super.accelerate();
    super.accelerate();
  }

  toString(){
    return "Faster car!";
  }
}
  const electricCar = new ElectricCar();

  electricCar.accelerate();
  electricCar.accelerate();
  electricCar.brake();
  console.log(electricCar.toString());

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()

/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 * 
 * 
 */



//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
/**let Car = class {
  constructor() {
    this.speed = 0;
  }

  accelerate(mph) {
    this.speed += mph;
  }

  brake(mph){
      this.speed -= mph;
  }
};

const car = new Car();

car.accelerate(60);
car.accelerate(60);

car.brake(60);

console.log("Speed is " + car.speed + " mph")
*/
